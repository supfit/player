// 1.歌曲搜索接口
// 请求地址：https://autumnfish.cn/search
// 请求方法:get
// 请求参数:keywords
// 响应内容：歌曲搜索结果

// 2.歌曲url获取接口
// 请求地址https://autumnfish.cn/song/url
// 请求方法:get
// 请求参数:id(查询关键字)
// 响应内容:歌曲url地址

//3.歌曲详情获取
// 请求地址：https://autumnfish.cn/song/detail
// 请求方法：get
// 请求参数：ids（歌曲id）
// 响应内容：歌曲详情（包括封面信息）

//4.热门评论获取
// 请求地址：https://autumnfish.cn/comment/hot?type=0
// 请求方法：get
// 请求参数：id（歌曲id,地址中的type固定为0）
// 响应内容：歌曲的热门评论

//5.mv地址获取
//请求地址：https://autumnfish.cn/mv/url
// 请求方法：get
// 请求参数：id（mvid,0表示没有）
// 响应内容：mv的地址

var app = new Vue({
    el:"#app",
    data:{
        name:"华晨宇",
        musicSong:[],
        musicUrl:"",
        musicCover:"",
        hotComments:[],
        isPlaying:false,
        isShow:false,
        mvUrl:""
    },
    methods:{
        search:function(){
            var that = this;
            axios.get("https://autumnfish.cn/search?limit=50&keywords="+this.name)
            .then(function(response){ 
                // console.log(response);
                that.musicSong=response.data.result.songs;
            },function(err){
                console.log(err);
            })
        },
        playMusic:function(musicId){
            var that = this;
            axios.get("https://autumnfish.cn/song/url?id="+musicId)
            .then(function(response){
                // console.log(reponse.data.data[0].url);
                that.musicUrl=response.data.data[0].url;
            },function(err){
                console.log(err);
            })
            //歌曲详情获取
            axios.get("https://autumnfish.cn/song/detail?ids="+musicId)
            .then(function(response){
                // console.log(response);
                that.musicCover=response.data.songs[0].al.picUrl
            },function(err){
                console.log(err);
            })

            //歌曲评论获取
            axios.get("https://autumnfish.cn/comment/hot?type=0&id="+musicId)
            .then(function(response){
                // console.log(response.data.hotComments);
                that.hotComments=response.data.hotComments;
            },function(err){
                console.log(err);
            })
        },
        play:function(){//播放
            console.log("播放");
            this.isPlaying=true;
        },
        pause:function(){//暂停
            console.log("暂停");
            this.isPlaying=false;
        },
        //播放mv
        playMv:function(mvid){
            var that = this;
            axios.get("https://autumnfish.cn/mv/url?id="+mvid)
            .then(function(response){
                // console.log(response.data.data.url);
                that.mvUrl=response.data.data.url;
                that.isShow=true;
            },function(err){
                console.log(err);
            })
        },
        //隐藏
        hide:function(){
            this.isShow=false;
            this.mvUrl="";
        }
    }
})